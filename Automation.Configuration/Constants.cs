﻿namespace Automation.Configuration
{
	public static class Constants
	{
		public const string DEFAULT_USERNAME = "fdk21069@hotmail.com";
		public const string DEFAULT_PASSWORD = "Moskva#98";
		
		public const string UpperCaseChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		public const string LowerCaseChars = "abcdefghijklmnopqrstuvwxyz";

		public const string SpecialChars = "!@#$%^*()_-+=[{]};:|./?";
		public const string DirectoryNameSpecialChars = "!@#$%^()_-+=[{]};.";
		public const string NumericChars = "0123456789";

		public static readonly string AlphanumericChars = $"{UpperCaseChars}{LowerCaseChars}{NumericChars}";
		public static readonly string Chars = $"{AlphanumericChars}{SpecialChars}";
		public static readonly string HtmlChars = $"{Chars}&',<>\\`~";

		public static readonly string ReportFolderName = "Reports";
		public static readonly string PngFileExt = ".png";
	}
}