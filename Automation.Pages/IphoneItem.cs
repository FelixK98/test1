﻿using OpenQA.Selenium;

namespace Automation.Pages
{
	public class IphoneItem
	{
		private IWebElement GridTile { get; }
		private string ItemPrice { get; }
		private string ItemName { get; }
		private IWebElement AddToCartButton { get; }
		private IWebElement AddToMyListButton { get; }
		private IWebElement AddToCompareButton { get; }

		public IphoneItem(IWebElement gridTile)
		{
			GridTile = gridTile;
			ItemPrice = gridTile.FindElement(By.XPath("//div[contains(@class,'ProductPrice')]")).Text;
			ItemName = gridTile.FindElement(By.XPath("//a")).GetAttribute("title");
			AddToCartButton = gridTile.FindElement(By.XPath("//button[@label='Add to Cart']"));
			AddToMyListButton = gridTile.FindElement(By.XPath("//button[@label='Add to My List']"));
			AddToCompareButton = gridTile.FindElement(By.XPath("//button[@label='Add to Compare']"));
		}

		public void AddItenToCart()
		{
			AddToCartButton.Click();
		}

		public void AddItemToWishList()
		{
			AddToMyListButton.Click();
		}

		public void AddItemToCompareList()
		{
			AddToCompareButton.Click();
		}

		public string GetItemName()
		{
			return ItemName;
		}

		public string GetItemPrice()
		{
			return ItemPrice;
		}
	}
}
