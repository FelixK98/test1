﻿using System;
using System.Collections.Generic;
using System.Linq;
using Automation.Core.SeleniumUtility;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Automation.Pages
{
	public class IphonesPage : BasePage
	{
		public IphonesPage(IExtendedWebDriver webDriver, Options options) : base(webDriver)
		{
			PageTitleName = "IPhones";
			PageUrl = new Uri(options.SiteUri, "shop/officeworks/c/technology/iphones-mobile-phones/iphones").ToString();
		}

		public string PageTitleName { get; set; }

		public List<IphoneItem> IphoneItems => WebDriver
			.FindElements(By.XPath("//div[@data-ref='tile-grid-container']/div[contains(@data-ref,'product-tile')]"))
			.Select(tile => new IphoneItem(tile)).ToList();

		public List<MiniCartItem> MiniCartItems => WebDriver
			.FindElements(By.XPath("//div[@data-ref='mini-cart']//div[contains(@class,'MiniCart__ItemContainer')]"))
			.Select(item => new MiniCartItem(item)).ToList();

		[FindsBy(How = How.XPath, Using = "//div[@data-ref='mini-cart']//button[text()='View Cart & Checkout']")]
		private IWebElement OpenCartButton { get; set; }

		[FindsBy(How = How.XPath, Using = "//div[@data-ref='navLink-cart']")]
		private IWebElement MenuItemCart { get; set; }

		[FindsBy(How = How.XPath, Using = "//div[@data-ref='navLink-cart']//div[@data-ref='counter']")]
		private IWebElement UserCartCounter { get; set; }

		public void OpenUserCart()
		{
			WebDriver.WaitUntilElementExists(OpenCartButton);
			OpenCartButton.Click();
		}

		public string GetCartCounter()
		{
			WebDriver.WaitUntilElementExists(MenuItemCart);
			return UserCartCounter.Text;
		}

		public void SelectCart()
		{
			SelectMenuItem(MenuItemCart);
		}

		private void SelectMenuItem(IWebElement element)
		{
			WebDriver.WaitUntilElementExists(element);
			element.Click();
		}
	}
}
