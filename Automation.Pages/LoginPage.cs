﻿using System;
using Automation.Core.SeleniumUtility;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace Automation.Pages
{
	public class LoginPage : BasePage
	{
		public LoginPage(IExtendedWebDriver webDriver, Options options) : base(webDriver)
		{
			PageTitleName = "";
			PageUrl = new Uri(options.SiteUri, "").ToString();
		}

		public string PageTitleName { get; set; }

		[FindsBy(How = How.Name, Using = "username")]
		private IWebElement Username { get; set; }

		[FindsBy(How = How.Name, Using = "password")]
		private IWebElement Password { get; set; }

		[FindsBy(How = How.XPath, Using = "//button[@data-ref='home-login-button']")]
		private IWebElement LogInButton { get; set; }

		public void EnterUsername(string username)
		{
			WebDriver.WaitUntilElementExists(Username);
			Username.SendKeys(username);
		}

		public void EnterPassword(string userPassword)
		{
			WebDriver.WaitUntilElementExists(Password);
			Password.SendKeys(userPassword);
		}

		public void SubmitLoginRequest()
		{
			WebDriver.WaitUntilElementExists(LogInButton);
			LogInButton.Click();
		}
	}
}
