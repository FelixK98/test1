﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Automation.Pages
{
	public class MiniCartItem
	{
		public IWebElement ItemRow { get; }
		public string ItemName { get; }

		public MiniCartItem(IWebElement element)
		{
			ItemRow = element;
			ItemName = element.FindElement(By.XPath("")).GetAttribute("title");
		}
	}
}
