﻿using Automation.Core.SeleniumUtility;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Linq;

namespace Automation.Pages
{
	public class UserCartPage : BasePage
	{
		public UserCartPage(IExtendedWebDriver webDriver, Options options) : base(webDriver)
		{
			PageTitleName = "IPhones";
		}

		public string PageTitleName { get; set; }

		public List<CartItem> CartItems => WebDriver
			.FindElements(By.XPath(""))
			.Select(tile => new CartItem(tile)).ToList();
	}
}
