﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace Automation.Pages
{
	public static class WebElementExtensions
	{
		public static void SelectByValue(this IWebElement element, string text)
		{
			var oSelect = new SelectElement(element);
			oSelect.SelectByValue(text);
		}

		public static string GetSelectedOption(this IWebElement element)
		{
			var oSelect = new SelectElement(element);
			return oSelect.SelectedOption.Text;
		}

		public static List<string> GetAllSelectOptions(this IWebElement element)
		{
			var oSelect = new SelectElement(element);
			var selectOptionsList = oSelect.Options;
			List<string> allSelectionOptions = new List<string>();

			foreach (var selectOption in selectOptionsList)
			{
				allSelectionOptions.Add(selectOption.Text);
			}

			return allSelectionOptions;
		}

	}
}
