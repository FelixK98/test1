﻿using System;
using System.Net;
using System.Text;
using Automation.Configuration;
using Newtonsoft.Json;
using RestSharp;
using Automation.Shared.Helpers;
using Automation.Repositories.Enum;

namespace Automation.Repositories
{
	public abstract class BaseApiClient
	{
		protected Uri BaseApiUri = Settings.SiteUrl;

		public AuthenticationType AuthType { get; set; }

		protected string Username { get; set; }
		protected string Password { get; set; }
		protected string ApiKey { get; set; }

		protected BaseApiClient()
		{
			AuthType = AuthenticationType.NoAuthentication;
		}

		protected BaseApiClient(string username, string password)
		{
			AuthType = AuthenticationType.Basic;
			Username = username;
			Password = password;
		}

		protected BaseApiClient(string apiKey)
		{
			AuthType = AuthenticationType.APIKey;
			ApiKey = apiKey;
		}

		protected string BuildBasicEncoding()
		{
			return Convert.ToBase64String(Encoding.GetEncoding("ISO-8859-1").GetBytes($"{Username}:{Password}"));
		}

		protected RestClient CreateRestClient(string endpoint)
		{
			return new RestClient($"{BaseApiUri.AbsoluteUri}{endpoint}");
		}

		protected void AddAuthorization(RestRequest request)
		{
			switch (AuthType)
			{
				case AuthenticationType.Basic:
					request.AddHeader("authorization", $"Basic {BuildBasicEncoding()}");
					break;

				case AuthenticationType.APIKey:
					if (DataHelper.GenerateRandomBoolean())
						request.AddHeader("api-key", ApiKey);
					else
						request.AddHeader("authorization", $"apikey {ApiKey}");
					break;

				case AuthenticationType.NoAuthentication:
					break;
			}
		}

		protected IRestResponse Post(string apiEndpoint, object requestBody)
		{
			var client = CreateRestClient(apiEndpoint);
			var request = new RestRequest(Method.POST);
			AddAuthorization(request);
			request.AddHeader("content-type", "application/json; charset=utf-8");
			request.AddParameter("application/json", JsonConvert.SerializeObject(requestBody),
				ParameterType.RequestBody);
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			return client.Execute(request);
		}

		protected IRestResponse Get(string apiEndpoint)
		{
			var client = CreateRestClient(apiEndpoint);
			var request = new RestRequest(Method.GET);
			AddAuthorization(request);
			request.AddHeader("content-type", "application/json; charset=utf-8");
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			return client.Execute(request);
		}

		protected IRestResponse Delete(string apiEndpoint)
		{
			var client = CreateRestClient(apiEndpoint);
			var request = new RestRequest(Method.DELETE);
			AddAuthorization(request);
			request.AddHeader("content-type", "application/json; charset=utf-8");
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			return client.Execute(request);
		}

		protected IRestResponse Put(string apiEndpoint, Object requestBody)
		{
			var client = CreateRestClient(apiEndpoint);
			var request = new RestRequest(Method.PUT);
			AddAuthorization(request);
			request.AddHeader("content-type", "application/json; charset=utf-8");
			request.AddParameter("application/json", JsonConvert.SerializeObject(requestBody),
				ParameterType.RequestBody);
			ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

			return client.Execute(request);
		}

	}
}