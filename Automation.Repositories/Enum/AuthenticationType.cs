﻿namespace Automation.Repositories.Enum
{
	public enum AuthenticationType
	{
		NoAuthentication,
		APIKey,
		Basic
	}
}
