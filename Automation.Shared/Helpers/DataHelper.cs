﻿using System;
using System.Linq;
using Automation.Configuration;

namespace Automation.Shared.Helpers
{
	public class DataHelper
	{
		private static readonly Random Random = new Random();

		public static bool GenerateRandomBoolean()
		{
			return Random.NextDouble() > 0.5;
		}

		public static string GenerateRandomPassword(int minLength = 6, int maxLength = 32)
		{
			// At least 6, but no more than 32 characters.
			// At least one UPPERCASE letter.
			// At least one lowercase letter.
			// At least one special character.
			// At least one number.
			var charArray = new char[4];
			charArray[0] = Constants.UpperCaseChars[Random.Next(0, Constants.UpperCaseChars.Length)];
			charArray[1] = Constants.LowerCaseChars[Random.Next(0, Constants.LowerCaseChars.Length)];
			charArray[2] = Constants.NumericChars[Random.Next(0, Constants.NumericChars.Length)];
			charArray[3] = Constants.SpecialChars[Random.Next(0, Constants.SpecialChars.Length)];

			var length = Random.Next(minLength - 4, maxLength - 4 + 1);
			return new string(Enumerable.Repeat(Constants.HtmlChars, length)
				.Select(s => s[Random.Next(s.Length)]).ToArray()) + new string(charArray);
		}

		public static string GenerateRandomAlphanumericString(int minLength = 2, int maxLength = 20)
		{
			var length = Random.Next(minLength, maxLength + 1);
			return new string(Enumerable.Repeat(Constants.AlphanumericChars, length)
				.Select(s => s[Random.Next(s.Length)]).ToArray());
		}

		public static string GenerateRandomString(int minLength, int maxLength, string characterSet = null)
		{
			var length = Random.Next(minLength, maxLength + 1);
			return new string(Enumerable.Repeat(string.IsNullOrEmpty(characterSet) ? Constants.Chars : characterSet, length)
				.Select(s => s[Random.Next(s.Length)]).ToArray());
		}

		public static int GenerateRandomInteger(int min, int max, int[] excludeNumbers = null)
		{
			if (excludeNumbers == null)
				excludeNumbers = new int[0];

			var result = Random.Next(min, max + 1);

			while (excludeNumbers.Contains(result))
				result = Random.Next(min, max + 1);

			return result;
		}

		public static TEnum SelectRandomEnumValue<TEnum>()
		{
			var values = Enum.GetValues(typeof(TEnum));
			return (TEnum)values.GetValue(Random.Next(values.Length));
		}

		public static double GenerateRandomDecimal(double minValue = Double.MinValue, double maxValue = Double.MaxValue)
		{
			var next = Random.NextDouble();
			return minValue + next * (maxValue - minValue);
		}

		public static string GenerateRandomEmailAddress(string userName = null)
		{
			if (userName == null)
				userName = GenerateRandomAlphanumericString();

			string[] extensions = {".com", ".org", ".net", ".com.au"};

			var domainPart = GenerateRandomAlphanumericString(4) + extensions[Random.Next(extensions.Length)];

			return userName + "@" + domainPart;
		}
	}
}