﻿using System;
using System.Collections.Generic;
using System.Linq;
using Should;

namespace Automation.Tests.Extensions
{
	public static class CollectionAssertions
	{
		public static void ShouldEqualSequence<TSource>(this IEnumerable<TSource> actual, IEnumerable<TSource> expected)
		{
			// Use of ToArray here is to ensure a nice output is shown when the values do not match
			actual.ToArray().ShouldEqual(expected.ToArray());
		}

		/// <summary>
		/// Compares two enumerables for equality using Should's ShouldEqual, ignoring order.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="actual"></param>
		/// <param name="expected"></param>
		/// <param name="orderBy"></param>
		public static void ShouldMatchUnordered<T>(this IEnumerable<T> actual, IEnumerable<T> expected, Func<T, T> orderBy = null)
		{
			orderBy = orderBy ?? (i => i);
			actual.OrderBy(orderBy).ToArray().ShouldEqual(expected.OrderBy(orderBy).ToArray());
		}
	}
}