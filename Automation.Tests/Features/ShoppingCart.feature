﻿@Chrome
Feature: Shopping Cart
As a User
I want to be able to see all items that I added to the cart
So that I can proceed with purchase

Scenario: Navigate to the close account screen
	Given I have added items to the cart
	When I select cart
	Then I see added items

