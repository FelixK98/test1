﻿using Automation.Configuration;
using Automation.Core.SeleniumUtility;
using Should;
using Automation.Pages;
using System;

namespace Automation.Tests.Procedures
{
	public class Navigator
	{
		private readonly LoginPage _loginPage;
		private IExtendedWebDriver _webDiver;

		public Navigator(LoginPage loginPage, IExtendedWebDriver webDiver)
		{
			_loginPage = loginPage;
			_webDiver = webDiver;
		}

		public void NavigateToRegister()
		{
			NavigateToSiteScreen();
		}

		public void NavigateToLogin()
		{
			NavigateToSiteScreen();
			_loginPage.VerifyPage().ShouldBeTrue();
		}

		public void LoginAs(string username, string password)
		{
			NavigateToLogin();
			_loginPage.VerifyPage().ShouldBeTrue();
			_loginPage.EnterUsername(username);
			_loginPage.EnterPassword(password);
			_loginPage.SubmitLoginRequest();
		}

		private void NavigateToSiteScreen()
		{
			_webDiver.NavigateTo(Settings.SiteUrl);
		}

		public void	NavigateToIphonesPage()
		{
			var targetURL =  new Uri(Settings.SiteUrl + "shop/officeworks/c/technology/iphones-mobile-phones/iphones");
			_webDiver.NavigateTo(targetURL);
		}

	}
}
