﻿using TechTalk.SpecFlow;

namespace WebUI.Automation.Tests.SUT
{
	public abstract class SUT<TUnderTest> where TUnderTest : new()
	{
		private static readonly string Key = typeof(TUnderTest).Name;

		public static TUnderTest GetScenarioInstance()
		{
			if (!ScenarioContext.Current.ContainsKey(Key))
			{
				ScenarioContext.Current.Add(Key, new TUnderTest());
			}
			return ScenarioContext.Current.Get<TUnderTest>(Key);
		}
	}
}