﻿namespace Automation.Tests.Steps
{
	public enum Pages
	{
		Main,
		Register,
		Login,
		MyAccounts,
		AccountDetails,
		DepositFunds,
		WithdrawFunds,
		TransferFunds,
		CloseAccount
	}
}