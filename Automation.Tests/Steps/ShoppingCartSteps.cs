﻿using Automation.Configuration;
using Automation.Core.SeleniumUtility;
using Automation.Pages;
using Automation.Tests.Procedures;
using TechTalk.SpecFlow;
using Should;
using Automation.Tests.Extensions;
using System.Collections.Generic;

namespace Automation.Tests.Steps
{
	[Binding]
    public class ShoppingCartSteps : StandardStepsBase
    {
		private readonly Navigator _navigator;
		private readonly IphonesPage _iphonesPage;

		private List<string> _itemNamesList = new List<string>();

		public ShoppingCartSteps(Navigator navigator, 
			IphonesPage iphonesPage,
			IExtendedWebDriver webDriver, Options options) : base (webDriver, options)
		{
			_navigator = navigator;
			_iphonesPage = iphonesPage;
		}

		[Given(@"I have added items to the cart")]
        public void GivenIHaveAddedItemsToTheCart()
        {
            _navigator.LoginAs(Constants.DEFAULT_USERNAME, Constants.DEFAULT_PASSWORD);
			_navigator.NavigateToIphonesPage();
			_iphonesPage.IphoneItems[0].AddItenToCart();
			_itemNamesList.Add(_iphonesPage.IphoneItems[0].GetItemName());
			_iphonesPage.IphoneItems[1].AddItenToCart();
			_itemNamesList.Add(_iphonesPage.IphoneItems[1].GetItemName());
		}

		[When(@"I select cart")]
        public void WhenISelectCart()
        {
			_iphonesPage.SelectCart();
        }
        
        [Then(@"I see added items")]
        public void ThenISeeAddedItems()
        {
			_iphonesPage.GetCartCounter().ShouldEqual("2");
			_iphonesPage.MiniCartItems.Count.ShouldEqual(2);

			List<string> itemNamesInCart = new List<string>
			{
				_iphonesPage.MiniCartItems[0].ItemName,
				_iphonesPage.MiniCartItems[1].ItemName
			};

			_itemNamesList.ShouldEqualSequence(itemNamesInCart);
		}
    }
}
