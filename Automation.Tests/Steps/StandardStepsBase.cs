﻿using Automation.Core.SeleniumUtility;
using Automation.Pages;

namespace Automation.Tests.Steps
{
	public abstract class StandardStepsBase
	{
		protected StandardStepsBase(IExtendedWebDriver webDriver, Options options)
		{
			WebDriver = webDriver;
			Options = options;
		}

		protected IExtendedWebDriver WebDriver { get; }

		protected Options Options { get; }
	}
}